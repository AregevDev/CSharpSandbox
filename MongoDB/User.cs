namespace MongoDB
{
    public struct User
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}