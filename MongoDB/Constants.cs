using MongoDB.Bson;
using MongoDB.Driver;

namespace MongoDB
{
    public static class Constants
    {
        public static readonly MongoClient Client =
            new MongoClient("mongodb+srv://admin:admin1@aregevdev-peftf.gcp.mongodb.net/test?retryWrites=true");

        public static readonly IMongoDatabase UsersDatabase = Client.GetDatabase("users");

        public static IMongoCollection<BsonDocument> GetUserCollection(string username)
        {
            return UsersDatabase.GetCollection<BsonDocument>(username);
        }
    }
}