using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MongoDB
{
    public static class Cli
    {
        public static void Run()
        {
            Console.WriteLine("Welcome to my MongoDB demo");
            var op = int.Parse(Console.ReadLine());
            switch (op)
            {
                case 0:
                    Register();
                    break;
                default:
                    Console.WriteLine("Unknown operation");
                    break;
            }
        }

        private static bool IsUserExists(string username)
        {
            var temp = new BsonDocument("name", username);
            var coll = Constants.UsersDatabase.ListCollections(new ListCollectionsOptions {Filter = temp});

            return coll.Any();
        }

        private static bool Register()
        {
            Console.WriteLine("Enter your desired Username");
            var username = Console.ReadLine();
            Console.WriteLine("Enter a Password");
            var pass1 = Console.ReadLine();
            Console.WriteLine("Retype your password");
            var pass2 = Console.ReadLine();

            if (pass1 != pass2) return false;
            Console.WriteLine();
            if (IsUserExists(username))
            {
                Console.WriteLine("Username already exists");
                return false;
            }

            var hash = CalculateHash(pass1);
            var doc = new BsonDocument
            {
                {"header", "basic"},
                {"username: ", username},
                {"password: ", hash}
            };
            Constants.GetUserCollection(username).InsertOne(doc);
            Console.WriteLine("Registered successfully");
            return true;
        }

        private static string CalculateHash(string str)
        {
            var message = Encoding.Unicode.GetBytes(str);
            var algo = new SHA256Managed();
            var hash = algo.ComputeHash(message);

            return hash.Aggregate("", (current, x) => current + $"{x:x2}");
        }
    }
}